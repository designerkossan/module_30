import pytest


# Тестирование всех GET-методов
@pytest.mark.parametrize("endpoint", ["/clients", "/clients/1"])
def test_get_endpoints_return_200(client, app, endpoint):
    response = client.get(endpoint)
    assert response.status_code == 200


def test_create_client(client, app):
    data = {
        "name": "NewClient",
        "surname": "NewSurname",
        "credit_card": "1234567890",
        "car_number": "XYZ789",
    }
    response = client.post("/clients", json=data)
    assert response.status_code == 201

    # Проверим, что клиент успешно создан
    # и его данные доступны через GET-запрос
    response = client.get("/clients")
    client_list = response.json
    assert len(client_list) > 0
    assert any(client["name"] == "NewClient" for client in client_list)


def test_create_parking(client, app):
    data = {
        "address": "NewParking",
        "opened": True,
        "count_places": 20,
        "count_available_places": 20,
    }
    response = client.post("/parkings", json=data)
    assert response.status_code == 201


@pytest.mark.parking
def test_entry_to_parking(client, app):
    # Тест на заезд на парковку
    data = {"client_id": 1, "parking_id": 1}
    response = client.post("/client_parkings", json=data)
    assert response.status_code == 201

    # Проверим соответствующие атрибуты
    client_data = client.get("/clients/1").json
    parking_data = client.get("/parkings/1").json

    assert parking_data["opened"] is True
    assert parking_data["count_available_places"] == 9
    assert client_data["credit_card"] is not None


@pytest.mark.parking
def test_exit_from_parking(client, app):
    # Тест на выезд с парковки
    entry_data = {"client_id": 1, "parking_id": 1}
    client.post("/client_parkings", json=entry_data)
    exit_data = {"client_id": 1, "parking_id": 1}
    response = client.delete("/client_parkings", json=exit_data)
    assert response.status_code == 200

    # Проверим соответствующие атрибуты
    parking_data = client.get("/parkings/1").json

    assert parking_data["count_available_places"] == 10
