import datetime

import pytest

from hw.main.api import app as flask_app
from hw.main.models import Client, ClientParking, Parking
from hw.main.models import db as _db


@pytest.fixture
def app():
    app = flask_app
    app.config["TESTING"] = True

    with app.app_context():
        _db.create_all()

        # Создаем тестового клиента
        client = Client(
            name="TestClient",
            surname="TestSurname",
            credit_card="1234567890",
            car_number="ABC123",
        )
        _db.session.add(client)

        # Создаем тестовую парковку
        parking = Parking(
            address="TestParking",
            opened=True,
            count_places=10,
            count_available_places=10,
        )
        _db.session.add(parking)

        # Создаем тестовый парковочный лог с фиксацией времени въезда и выезда
        entry = ClientParking(
            client_id=client.id,
            parking_id=parking.id,
            time_in=datetime.datetime.utcnow(),
        )
        _db.session.add(entry)

        _db.session.commit()
        yield app

    with app.app_context():
        _db.drop_all()


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


@pytest.fixture
def db(app):
    with app.app_context():
        yield _db
