from hw.main.models import Client, Parking
from hw.test.factories import ClientFactory, ParkingFactory


def test_create_client(client, db):
    # Создание клиента с использованием фабрики
    client = ClientFactory.create()

    # Проверка, что объект клиента был создан
    assert isinstance(client, Client)

    # Проверка, что поля имени и фамилии не пусты
    assert client.name
    assert client.surname

    # Проверка, что поле номера кредитной карты не пусто
    assert client.credit_card

    # Проверка, что поле номера автомобиля не пусто
    assert client.car_number


def test_create_parking(client, db):
    # Создание парковки с использованием фабрики
    parking = ParkingFactory.create()

    # Проверка, что объект парковки был создан
    assert isinstance(parking, Parking)

    # Проверка, что поле адреса не пусто
    assert parking.address

    # Проверка, что поле opened является булевым значением (True или False)
    assert parking.opened in [True, False]

    # Проверка, что поле count_places больше нуля
    assert parking.count_places > 0

    # Проверка, что поле count_available_places
    # равно значению count_places (по умолчанию)
    assert parking.count_available_places == parking.count_places
