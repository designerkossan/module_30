from flask import Flask

from hw.main.models import db


def create_app(config_filename):
    app = Flask(__name__)

    # Загружаем конфигурацию из файла
    app.config.from_pyfile(config_filename)

    # Инициализируем SQLAlchemy
    db.init_app(app)

    with app.app_context():
        db.create_all()

    return app


if __name__ == "__main__":
    app = create_app("config.py")
    app.run(debug=True)
