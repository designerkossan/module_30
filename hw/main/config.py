# Настройки для базы данных
SQLALCHEMY_DATABASE_URI = "sqlite:///parking.db"
SQLALCHEMY_TRACK_MODIFICATIONS = False  # Отключаем отслеживание изменений
SECRET_KEY = "my_super_secret_key"
