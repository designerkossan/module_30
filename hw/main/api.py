from flask import request
from flask_restful import Api, Resource

from hw.main.app import create_app
from hw.main.models import Client, ClientParking, Parking, db

app = create_app("config.py")
api = Api(app)


class ClientResource(Resource):
    def get(self):
        clients = Client.query.all()
        return [
            {
                "id": c.id,
                "name": c.name,
                "surname": c.surname,
                "credit_card": c.credit_card,
                "car_number": c.car_number,
            }
            for c in clients
        ]

    def post(self):
        data = request.get_json()
        new_client = Client(
            name=data["name"],
            surname=data["surname"],
            credit_card=data["credit_card"],
            car_number=data["car_number"],
        )
        db.session.add(new_client)
        db.session.commit()
        return {"message": "Client created successfully"}, 201


class ClientDetailResource(Resource):
    def get(self, client_id):
        client = db.session.get(Client, client_id)
        if client:
            return {
                "id": client.id,
                "name": client.name,
                "surname": client.surname,
                "credit_card": client.credit_card,
                "car_number": client.car_number,
            }
        return {"message": "Client not found"}, 404


class ParkingResource(Resource):
    def get(self):
        parkings = Parking.query.all()
        return [
            {
                "id": p.id,
                "address": p.address,
                "opened": p.opened,
                "count_places": p.count_places,
                "count_available_places": p.count_available_places,
            }
            for p in parkings
        ]

    def post(self):
        data = request.get_json()
        new_parking = Parking(
            address=data["address"],
            opened=data["opened"],
            count_places=data["count_places"],
            count_available_places=data["count_places"],
        )
        db.session.add(new_parking)
        db.session.commit()
        return {"message": "Parking created successfully"}, 201


class ParkingDetailResource(Resource):
    def get(self, parking_id):
        parking = db.session.get(Parking, parking_id)
        if parking:
            return {
                "id": parking.id,
                "address": parking.address,
                "opened": parking.opened,
                "count_places": parking.count_places,
                "count_available_places": parking.count_available_places,
            }
        return {"message": "Client not found"}, 404


class ClientParkingResource(Resource):
    def post(self):
        data = request.get_json()
        client_id = data["client_id"]
        parking_id = data["parking_id"]

        client = db.session.get(Client, client_id)
        parking = db.session.get(Parking, parking_id)

        if not client or not parking:
            return {"message": "Client or parking not found"}, 404

        if not parking.opened:
            return {"message": "Parking is not open"}, 400

        if parking.count_available_places > 0:
            new_entry = ClientParking(
                client_id=client_id,
                parking_id=parking_id,
                time_in=db.func.current_timestamp(),
            )
            parking.count_available_places -= 1
            db.session.add(new_entry)
            db.session.commit()
            return {"message": "Entry recorded successfully"}, 201
        return {"message": "No available parking spots"}, 400

    def delete(self):
        data = request.get_json()
        client_id = data["client_id"]
        parking_id = data["parking_id"]

        entry = ClientParking.query.filter_by(
            client_id=client_id, parking_id=parking_id, time_out=None
        ).first()
        if entry:
            entry.time_out = db.func.current_timestamp()
            parking = db.session.get(Parking, parking_id)
            parking.count_available_places += 1
            db.session.commit()
            return {"message": "Exit recorded successfully"}, 200
        return {"message": "Entry not found"}, 404


api.add_resource(ClientResource, "/clients")
api.add_resource(ClientDetailResource, "/clients/<int:client_id>")
api.add_resource(ParkingResource, "/parkings")
api.add_resource(ParkingDetailResource, "/parkings/<int:parking_id>")
api.add_resource(ClientParkingResource, "/client_parkings")


if __name__ == "__main__":
    app.run(debug=True)
